### What is application doing

This application solves packing problem using two algorithms: the brute force where all possible combinations are 
checked and fast algorithm which might not always give the most optimal result but it is O(log n) complex.
So the problem is to pack the package with products which total cost is the highest but package capacity limit is not
exceeded. Program reads the file for input data each line is a test case. Line consist of package capacity(max weight)
and (after colon) list of products with following attributes: index, weight, price in that order. For sample input
file see src/resources/input.txt