package com.mlis.service;

import com.mlis.entity.Package;

/**
 * abstraction for packing algorithm
 */
public interface PackingStrategyInterface {

    /**
     * represents packing algorithm
     * @param inputPackage package to be packed
     */
    void pack(Package inputPackage);
}
