package com.mlis.service;

import com.mlis.entity.Package;
import com.mlis.entity.Product;
import com.mlis.util.Combination;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * represents algorithm with all possibilities check(all combinations of products in package)
 * the performance is low but could be useful for small data sets since optimal result is guaranteed
 *
 * @// TODO: 21.08.18 test
 */
public class BruteForcePacking implements PackingStrategyInterface {

    private List<Product> products;

    /**
     *
     * @param products products to be packed
     *                 for better performance ArrayList could be passed here
     *                 since algorithm uses lof of read operations
     */
    public BruteForcePacking(List<Product> products) {

        this.products = products;
    }

    /**
     * packs given package using brute force method
     * in order to determine most optimal package content
     *
     * @param inputPackage package to be packed
     */
    @Override
    public void pack(Package inputPackage) {

        HashSet<Integer[]> allCombinations = this.findAllValidCombinations(inputPackage);

        // there might be no results due to invalid or no matching criteria input data
        if (allCombinations.isEmpty())
            return;

        Integer[] bestCombination = this.findBestMatchingCombination(allCombinations);

        for (int index : bestCombination) {
            inputPackage.addProduct(this.findByIndex(index));
        }
    }

    /**
     * makes all possible products combinations and chooses those
     * which total weight not exceeding package capacity
     *
     * @param inputPackage
     * @return
     */
    private HashSet<Integer[]> findAllValidCombinations(Package inputPackage) {

        HashSet<Integer[]> allCombinations = new HashSet<>();

        for (int i = products.size(); i > 0; i--) {

            HashSet<Integer[]> kCombinations = Combination.find(getIndexes(), i);
            HashSet<Integer[]> combinations = this.filterNonFittingCombinations(kCombinations, inputPackage);

            allCombinations.addAll(combinations);
        }

        return allCombinations;
    }

    /**
     * compares total cost of products represented by given indexes set and
     * chooses the set with the highest value
     *
     * @param combinations combination of product indexes
     * @return
     */
    private Integer[] findBestMatchingCombination(HashSet<Integer[]> combinations) {

        return combinations.stream().max(Comparator.comparing(comb -> {
            float cost = 0;
            for (Integer index : comb) {
                Product product = this.findByIndex(index);
                cost += product.getPrice();
            }

            return cost;

        })).get();
    }

    /**
     * filters product combinations which in total exceed given package capacity
     *
     * @param combinations
     * @param inputPackage
     * @return
     */
    private HashSet<Integer[]> filterNonFittingCombinations(HashSet<Integer[]> combinations, Package inputPackage) {

        return combinations.stream().filter(ints -> {

            Package tmpPackage = inputPackage.clone();

            for (Integer index : ints) {
                Product product = this.findByIndex(index);

                if (!tmpPackage.isFitting(product)) {
                    return false;
                }

                tmpPackage.addProduct(product);
            }
            return true;

        }).collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * search for product object with given index value in the
     * list of all input {@link BruteForcePacking::products} to be packed
     *
     * @param index
     * @return
     */
    private Product findByIndex(int index) {

        Optional<Product> product = products.stream().filter(p -> p.getIndex() == index).findFirst();

        return product.get();
    }

    /**
     * extracts product indexes
     *
     * @return
     */
    private List<Integer> getIndexes() {

        return products.stream().map(Product::getIndex).collect(Collectors.toList());
    }
}
