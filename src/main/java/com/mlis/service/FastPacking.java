package com.mlis.service;

import com.mlis.entity.Package;
import com.mlis.entity.Product;

import java.util.Comparator;
import java.util.List;

/**
 * represents packing algorithm
 * first products are sorted according to price/weight ratio
 * then starting from highest ratio products are added to package in case product weight
 * not exceeding total package weight limit, if it does next product on the list is taken
 */
public class FastPacking implements PackingStrategyInterface {

    private List<Product> products;

    /**
     *
     * @param products products to be packed
     */
    public FastPacking(List<Product> products) {

        this.products = products;

        //sort immediately to not repeat it in case of multiple call of pack method within same object
        this.sort();
    }

    /**
     * packs Package based on price to weight ratio
     * note that it might not always give the most optimal solution
     *
     * @param inputPackage package to be packed
     */
    @Override
    public void pack(Package inputPackage) {

        for (Product product : this.products) {

            // only add product if fits to package capacity
            if (inputPackage.isFitting(product)) {
                inputPackage.addProduct(product);
            }

            if (inputPackage.isFull()) {
                break;
            }
        }
    }

    /**
     * sort products descending based on ratio price/weight if ratio
     * is same for multiple products smaller weight is taken in favour
     */
    private void sort() {
        Comparator<Product> ratioComparator = Comparator.comparing((p -> p.getPrice() / p.getWeight()));
        Comparator<Product> comparator = ratioComparator.thenComparing(Product::getWeight);

        products.sort(comparator.reversed());
    }
}
