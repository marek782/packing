package com.mlis.service;

import com.mlis.entity.Package;
import com.mlis.entity.PackingData;
import com.mlis.entity.Product;
import com.mlis.exception.ApiException;
import com.mlis.exception.InvalidFileDataException;

import java.util.LinkedList;

/**
 * concrete implementation of converting input data to domain objects
 * this class coverts string typically retrieved from file, accepts following format of file line:
 * 81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
 * the amount of white spaces in given sample is irrelevant
 *
 */
public class TxtFileDataConverter implements RowConverterInterface {

    // exception messages
    public final String INVALID_PRODUCT_DATA_FORMAT = "Invalid product data format";
    public final String EXPECTED_PACKAGE_DELIMITER_NOT_FOUND = "Expected package delimiter not found";

    // delimiters for string parsing purposes
    final String PACKAGE_DELIMITER = "\\s*:\\s*";
    final String PRODUCTS_DELIMITER = "\\s+";
    final String PRODUCT_DATA_DELIMITER = ",";


    /**
     * converts string(typically from file) line to domain object (the {@link PackingData} object)
     *
     * @param fileLine
     * @return
     * @throws InvalidFileDataException when values cannot be parsed or line format is
     *                                  different than accepted, see {@link TxtFileDataConverter} class
     *                                  description for accepted line format
     */
    public PackingData convert(String fileLine) throws InvalidFileDataException {

        FileLineData data = this.getLineData(fileLine);

        try {
            Package aPackage = new Package(Float.parseFloat(data.aPackage));
            LinkedList<Product> products = this.toProductsList(data.products.split(PRODUCTS_DELIMITER));

            return new PackingData(aPackage, products);

        } catch (NumberFormatException e) {
            throw new InvalidFileDataException("Cannot parse data: " + data, ApiException.EXC_CODE_0003);
        }
    }

    /**
     * makes object containing products data string and package weight string,
     * in other words divides given string line into part containing weight value
     * and part containing products list then returns them in object representation
     *
     * @param fileLine
     * @return
     * @throws InvalidFileDataException when file line format is invalid
     */
    private FileLineData getLineData(String fileLine) throws InvalidFileDataException {

        String data[] = fileLine.split(PACKAGE_DELIMITER);

        if (data.length != 2) {
            throw new InvalidFileDataException(EXPECTED_PACKAGE_DELIMITER_NOT_FOUND, ApiException.EXC_CODE_0001);
        }

        return new FileLineData(data[0], data[1]);
    }

    /**
     * makes list of Product objects from array of strings representing
     * product items
     *
     * @param products
     * @return
     * @throws InvalidFileDataException
     */
    private LinkedList<Product> toProductsList(String[] products) throws InvalidFileDataException {

        //linked list since mainly add operation is used
        LinkedList<Product> result = new LinkedList<>();

        for (String product : products) {

            String items[] = product.replaceAll("[()]", "").split(PRODUCT_DATA_DELIMITER);
            result.add(this.toProduct(items));
        }

        return result;
    }

    /**
     * build Product object from elements retrieved by
     * splitting product data string with specific delimiter
     *
     * @param productData it should be array of 3 elements: index, price, weight
     * @return
     * @throws InvalidFileDataException
     */
    private Product toProduct(String[] productData) throws InvalidFileDataException {

        if (productData.length != 3) {
            throw new InvalidFileDataException(INVALID_PRODUCT_DATA_FORMAT, ApiException.EXC_CODE_0002);
        }

        // get price starting from 1-st index to omit currency sign
        float price = Float.parseFloat(productData[2].substring(1));
        float weight = Float.parseFloat(productData[1]);
        int index = Integer.parseInt(productData[0]);

        return new Product(index, price, weight);
    }

    /**
     * structure which helps to avoid direct access to array elements after splitting string
     * like string[0] or string[1], gives meaningful properties instead
     */
    private class FileLineData {

        public String aPackage;
        public String products;

        public FileLineData(String aPackage, String products) {
            this.aPackage = aPackage;
            this.products = products;
        }

        public String toString() {
            return aPackage + products;
        }
    }
}
