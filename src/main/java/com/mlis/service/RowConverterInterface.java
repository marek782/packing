package com.mlis.service;

import com.mlis.entity.PackingData;

/**
 * abstraction for data row conversion
 * decouples domain object creations from reading data from source
 */
public interface RowConverterInterface {

    /**
     * transforms input data into domain object
     *
     * @param input
     * @return
     * @throws Exception when conversion is not possible
     */
    PackingData convert(String input) throws Exception;
}
