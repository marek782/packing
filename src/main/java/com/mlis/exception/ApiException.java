package com.mlis.exception;

public class ApiException extends Exception {

    /**
     * list of unique codes for each exception occurrence
     * for search purposes
     */
    public static final String EXC_CODE_0001 = "exc_code_0001";
    public static final String EXC_CODE_0002 = "exc_code_0002";
    public static final String EXC_CODE_0003 = "exc_code_003";
    public static final String EXC_CODE_0004 = "exc_code_004";

    protected String code;

    /**
     *
     * @param message
     * @param code unique code for specific exception occurrence
     */
    public ApiException(String message, String code) {
        super(message);
        this.code = code;
    }
}
