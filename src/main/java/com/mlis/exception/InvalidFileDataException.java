package com.mlis.exception;

/**
 * thrown when file contains invalid format of data
 */
public class InvalidFileDataException extends ApiException {

    /**
     *
     * @param message
     * @param code unique code for better occurrence searchability
     */
    public InvalidFileDataException(String message, String code) {
        super(message, code);
    }
}
