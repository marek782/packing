package com.mlis.packer;

import com.mlis.dao.DataReaderInterface;
import com.mlis.dao.FileDataReader;
import com.mlis.entity.Package;
import com.mlis.entity.PackingData;
import com.mlis.entity.Product;
import com.mlis.exception.ApiException;
import com.mlis.service.BruteForcePacking;
import com.mlis.service.FastPacking;
import com.mlis.service.PackingStrategyInterface;
import com.mlis.service.TxtFileDataConverter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

/**
 * main assignment task
 * packing facade - initializes dependencies and gives high level interface
 * for executing packing action
 *
 * @implNote : this is the entry point for application task, not a reusable component
 *            DI and formatting is intended here
 */
public class Packer {

    /**
     * perform packing with {@link FastPacking} algorithm
     *
     * @param filePath path to input file data
     * @return
     * @throws ApiException when given filePath doesn't exists
     */
    public static String pack(String filePath) throws ApiException {

        return Packer.pack(filePath, "fast");
    }

    /**
     * perform packing with given algorithm
     *
     * @param filePath
     * @param algorithm type of algorithm, available values: "fast", "brute-force"
     * @return
     * @throws ApiException when given filePath doesn't exist
     */
    public static String pack(String filePath, String algorithm) throws ApiException {

        int i = 0;

        StringBuilder output = new StringBuilder();
        FileDataReader dataReader = (FileDataReader) Packer.dataReaderFactory(filePath);
        LinkedList<PackingData> data = dataReader.load();

        for (PackingData pData : data) {

            Package aPackage = Packer.performPackingWithAlgorithm(pData, algorithm);
            Packer.formatOutput(output, aPackage, ++i);
        }

        return output.toString();
    }

    /**
     * default factory for dao object
     *
     * @param filePath
     * @return
     * @throws ApiException
     */
    public static DataReaderInterface dataReaderFactory(String filePath) throws ApiException {

        try {

            return new FileDataReader(filePath, new TxtFileDataConverter());

        } catch (FileNotFoundException e) {
            throw new ApiException("invalid path provided", ApiException.EXC_CODE_0004);
        }
    }

    /**
     * packing algorithm factory
     *
     * @param products
     * @param algorithm type of algorithm, available values: "fast", "brute-force"
     * @return
     */
    public static PackingStrategyInterface algorithmFactory(List<Product> products, String algorithm) {

        switch (algorithm) {
            case "fast":
                return new FastPacking(products);

            case "brute-force":
                return new BruteForcePacking(products);

            default:
                throw new NotImplementedException();
        }
    }

    /**
     * packs {@link Package} with given algorithm and returns reference to it
     *
     * @param data
     * @param algorithm
     * @return
     */
    private static Package performPackingWithAlgorithm(PackingData data, String algorithm) {

        PackingStrategyInterface packingAlg = Packer.algorithmFactory(data.getProducts(), algorithm);
        Package aPackage = data.getPackage();

        packingAlg.pack(aPackage);

        return aPackage;
    }

    /**
     *
     * @param builder
     * @param aPackage
     * @param index
     */
    private static void formatOutput(StringBuilder builder, Package aPackage, int index) {
        builder
                .append(index)
                .append("\n")
                .append("-\n")
                .append(aPackage.toString())
                .append("\n\n");
    }
}
