package com.mlis.dao;

import com.mlis.entity.PackingData;
import com.mlis.service.RowConverterInterface;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.LinkedList;

public class FileDataReader implements DataReaderInterface {

    private BufferedReader fileReader;
    private RowConverterInterface converter;

    /**
     *
     * @param filePath
     * @param converter determines how to parse file line
     * @throws FileNotFoundException
     */
    public FileDataReader(String filePath, RowConverterInterface converter) throws FileNotFoundException {
        this.fileReader = new BufferedReader(new FileReader(filePath));
        this.converter = converter;
    }

    /**
     * loads {@link PackingData} from file
     * returns only valid lines (lines which can be converted by converter)
     *
     * @return
     */
    public LinkedList<PackingData> load() {

        Iterator<String> iterator = this.fileReader.lines().iterator();
        LinkedList<PackingData> data = new LinkedList<>();

        while (iterator.hasNext()) {
            try {
                String line = iterator.next();
                data.add(this.converter.convert(line));

            } catch (Exception e) { /* todo log */}
        }

        return data;
    }
}
