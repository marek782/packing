package com.mlis.dao;

import com.mlis.entity.PackingData;

import java.util.List;

public interface DataReaderInterface {

    /**
     * returns aggregation of products and package data
     *
     * @return
     */
    List<PackingData> load();
}
