package com.mlis.entity;

import java.util.LinkedList;

/**
 * represents aggregation of single packing input data:
 * the package instance and list of products to be packed
 */
public class PackingData {

    private Package aPackage;
    private LinkedList<Product> products;

    /**
     *
     * @param aPackage
     * @param products
     */
    public PackingData(Package aPackage, LinkedList<Product> products) {
        this.aPackage = aPackage;
        this.products = products;
    }

    public Package getPackage() {
        return aPackage;
    }

    public LinkedList<Product> getProducts() {
        return products;
    }
}
