package com.mlis.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * domain entity
 * object representation of package
 */
public class Package {

    private float maxWeight;
    private List<Product> products = new ArrayList<>();

    public Package(float maxWeight) {
        this.maxWeight = maxWeight;
    }

    /**
     *
     * @param product
     * @return false when product cannot be added due to capacity(weight) limit
     *         true otherwise
     */
    public boolean addProduct(Product product) {

        if (this.isFitting(product)) {

            this.products.add(product);
            return true;
        }

        return false;
    }

    public double getTotalWeight() {
        return this.products.stream().mapToDouble(Product::getWeight).sum();
    }

    public double getTotalCost() {
        return this.products.stream().mapToDouble(Product::getPrice).sum();
    }

    public boolean isFull() {
        return this.maxWeight == this.getTotalWeight();
    }

    public boolean isFitting(Product p) {
        return p.getWeight() + this.getTotalWeight() <= this.maxWeight;
    }

    public List<Product> getProducts() {
        return products;
    }

    public float getMaxWeight() {
        return maxWeight;
    }

    public Package clone() {

        return new Package(this.maxWeight);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        this.products.forEach(product -> {
            builder.append(product.getIndex()).append(",");
        });

        String output = builder.toString();
        int length = output.length() > 0 ? output.length() - 1 : 0;

        //omit last comma
        return output.substring(0, length);
    }
}
