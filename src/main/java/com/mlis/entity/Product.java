package com.mlis.entity;

/**
 * product domain entity
 */
public class Product {

    private int index;
    private float price;
    private float weight;

    public Product(int index, float price, float weight) {
        this.index = index;
        this.price = price;
        this.weight = weight;
    }

    public int getIndex() {
        return index;
    }

    public float getPrice() {
        return price;
    }

    public float getWeight() {
        return weight;
    }
}
