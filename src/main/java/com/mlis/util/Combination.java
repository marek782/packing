package com.mlis.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class Combination {

    /**
     * returns all possible unique k-element combinations from data set
     * the order of elements in combination is irrelevant, for instance
     * combination [2,3,4] is considered same as [3,4,2] and not included in
     * returned value
     *
     * @param data data set
     * @param k combination size
     * @return
     */
    public static HashSet<Integer[]> find(List<Integer> data, int k) {

        // final result set
        HashSet<Integer[]> result = new HashSet<>();
        HashSet<Integer[]> pairs = new HashSet<>();

        for (Integer e: data) {

            // right element neighbours - rest of the elements on the right side from element position
            List<Integer> neighbours = new ArrayList<>(data.subList(data.indexOf(e) + 1, data.size()));

            if (k == 2) {
                // make 2 element combinations with element e and each of its right neighbour
                HashSet<Integer[]> elementPairs = makeElementPairs(e, neighbours);
                pairs.addAll(elementPairs);

            } else {
                /* make sub-combinations consisting of neighbours elements
                   then append element e to make k-element combination */
                HashSet<Integer[]> pairs2 = find(neighbours, k-1);
                HashSet<Integer[]> combinations = joinWithPairs(e, pairs2);
                result.addAll(combinations);
            }
        }

        /* when k == 2 then pairs is the result since paris has to be generated for all elements of data collection,
           returning in "if" will break before parent looping is completed, the pairs collection is returned for
           recursive call, the result is the final set of k-element combinations */
        return (k == 2) ? pairs : result;
    }


    /**
     * helper function to join input element to existing list
     *
     * @param element
     * @param pairs
     * @return
     */
    private static HashSet<Integer[]> joinWithPairs(Integer element, HashSet<Integer[]> pairs) {

        HashSet<Integer[]> result = new HashSet<>();

        for (Integer[] pair: pairs) {

            List<Integer> tmp = Arrays.stream(pair).collect(Collectors.toList());
            tmp.add(0, element);

            result.add(tmp.toArray(new Integer[0]));
        }

        return result;
    }

    /**
     * combines element with each element of given list
     * in other words function creates list of each pair combination of element with
     * all elements from neighbours list
     *
     * @param element
     * @param neighbours
     * @return
     */
    private static HashSet<Integer[]> makeElementPairs(Integer element, List<Integer> neighbours)
    {
        HashSet<Integer[]> pairs = new HashSet<>();

        for (Integer n: neighbours) {
            List<Integer> p = new ArrayList<>();
            p.add(element);
            p.add(n);

            pairs.add((p.toArray(new Integer[0])));
        }

        return pairs;
    }
}
