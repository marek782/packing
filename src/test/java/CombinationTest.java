import com.mlis.util.Combination;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CombinationTest extends TestCase {

    private int[] dataSet1 = {
            1,2,3,4
    };

    private int[] dataSet2 = {
            1,2,3,4,5
    };

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testDataSet1FindResult2k() {

        Integer[][] expected = {
                {2,4},
                {2,3},
                {3,4},
                {1,4},
                {1,3},
                {1,2}
        };

        List<Integer> data = Arrays.stream(dataSet1).boxed().collect(Collectors.toList());
        HashSet<Integer[]> result = Combination.find(data, 2);

        int sum1 = result.stream().mapToInt(value -> value[0] + value[1]).sum();
        int sum2 = Arrays.stream(expected).mapToInt(value -> value[0] + value[1]).sum();

        assertEquals(result.size(), expected.length);
        assertEquals(sum1, sum2);
    }

    @Test
    public void testDataSet1FindResult3k() {

        Integer[][] expected = {
                {1,2,3},
                {2,3,4},
                {1,3,4},
                {1,2,4}
        };

        List<Integer> data = Arrays.stream(dataSet1).boxed().collect(Collectors.toList());
        HashSet<Integer[]> result = Combination.find(data, 3);

        int sum1 = result.stream().mapToInt(value -> value[0] + value[1] + value[2]).sum();
        int sum2 = Arrays.stream(expected).mapToInt(value -> value[0] + value[1] + value[2]).sum();

        assertEquals(result.size(), expected.length);
        assertEquals(sum1, sum2);
    }

    @Test
    public void testDataSet2FindResult3k() {

        Integer[][] expected = {
                {1,2,3},
                {2,3,4},
                {2,4,5},
                {1,4,5},
                {1,3,5},
                {1,3,4},
                {1,2,4},
                {1,2,5},
                {2,3,5},
                {3,4,5}
        };

        List<Integer> data = Arrays.stream(dataSet2).boxed().collect(Collectors.toList());
        HashSet<Integer[]> result = Combination.find(data, 3);

        int sum1 = result.stream().mapToInt(value -> value[0] + value[1] + value[2]).sum();
        int sum2 = Arrays.stream(expected).mapToInt(value -> value[0] + value[1] + value[2]).sum();

        assertEquals(result.size(), expected.length);
        assertEquals(sum1, sum2);
    }
}
