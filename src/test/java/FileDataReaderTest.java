import com.mlis.dao.DataReaderInterface;
import com.mlis.dao.FileDataReader;
import com.mlis.entity.PackingData;
import com.mlis.service.TxtFileDataConverter;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

public class FileDataReaderTest extends TestCase {

    private DataReaderInterface fileReader;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        this.fileReader = new FileDataReader("src/test/resources/input.txt", new TxtFileDataConverter());
    }

    @Test
    public void testLoadResultOk() {

        List<PackingData> result = this.fileReader.load();
        assertEquals(3, result.size());
    }

}
