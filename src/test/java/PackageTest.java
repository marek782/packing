import com.mlis.entity.Package;
import com.mlis.entity.Product;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;

public class PackageTest extends TestCase {

    private Package aPackage;
    private Product[] products = {
            new Product(1, 5, 2),
            new Product(2, 3, 1),
            new Product(3, 6, 1),
            new Product(4, 6, 3)
    };

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        this.aPackage = new Package(9);
        Arrays.stream(this.products).forEach(product1 -> {
            this.aPackage.addProduct(product1);
        });
    }

    @Test
    public void testFittingOk() {

        Product product = new Product(7, 2, 2);

        assertEquals(true, this.aPackage.isFitting(product));
    }

    @Test
    public void testNotFitting() {

        Product product = new Product(7, 2, 3);

        assertEquals(false, this.aPackage.isFitting(product));
    }

    @Test
    public void testTotalCost() {

        assertEquals(20.0, this.aPackage.getTotalCost());
    }

    @Test
    public void testTotalWeight() {

        assertEquals(7.0, this.aPackage.getTotalWeight());
    }
}
