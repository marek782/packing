import com.mlis.entity.PackingData;
import com.mlis.service.RowConverterInterface;
import com.mlis.service.TxtFileDataConverter;
import junit.framework.TestCase;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TxtFileDataConverterTest extends TestCase {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private String validLine;
    private String invalidPackageSeparatorLine;
    private RowConverterInterface converter;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.validLine = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        this.invalidPackageSeparatorLine = "81 ; (5,30.18,€9) (6,46.34,€48)";
        this.converter = new TxtFileDataConverter();
    }

    @Test
    public void testConvertOk() throws Exception {

        PackingData result = this.converter.convert(this.validLine);

        assertEquals((float)81.0, result.getPackage().getMaxWeight());
        assertEquals(6, result.getProducts().size());
    }

    @Test
    public void testConvertInvalidInput() {

        String message = "";

        try {
            this.converter.convert(this.invalidPackageSeparatorLine);
        } catch (Exception e) {
            message = e.getMessage();
        }

        assertEquals(((TxtFileDataConverter) (this.converter)).EXPECTED_PACKAGE_DELIMITER_NOT_FOUND, message);
    }
}
